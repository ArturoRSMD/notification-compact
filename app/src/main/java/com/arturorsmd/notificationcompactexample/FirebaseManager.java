package com.arturorsmd.notificationcompactexample;

import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FirebaseManager extends FirebaseMessagingService {

    NotificationManagerCompat notificationManagerCompat;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        notificationManagerCompat = NotificationManagerCompat.from(this);
        basicNotification(ChannelManager.HIGH_IMPORTANCE_CHANNEL, remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());


    }

    public void basicNotification(String channelId, String contentTitle, String contentText){
        //Crear notificacion
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_backup)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setColor(Color.GREEN)
                .setAutoCancel(true);

        notificationManagerCompat.notify(01, notification.build());
    }
}
