package com.arturorsmd.notificationcompactexample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    NotificationManagerCompat notificationManagerCompat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        notificationManagerCompat = NotificationManagerCompat.from(this);

        //basicNotification(ChannelManager.HIGH_IMPORTANCE_CHANNEL);
        //bigTextStyleNotification(ChannelManager.HIGH_IMPORTANCE_CHANNEL);
        //bigPictureStyleNotification(ChannelManager.HIGH_IMPORTANCE_CHANNEL);
        inboxStyleNotification(ChannelManager.HIGH_IMPORTANCE_CHANNEL);

    }

    public void basicNotification(String channelId){
        //Crear notificacion
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_backup)
                .setContentTitle("Basic Notification")
                .setContentText("Basic Notification Description")
                .setColor(Color.GREEN)
                .setAutoCancel(true);

        notificationManagerCompat.notify(01, notification.build());
    }

    public void bigTextStyleNotification(String channelId){

        //Crear notificacion
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_backup)
                .setContentTitle("Basic Notification")
                .setContentText("Basic Notification Description")
                .setColor(Color.GREEN)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .setBigContentTitle("GoDaddy")
                        .bigText(getString(R.string.big_text))
                        .setSummaryText("Bandeja der entrada")
                );

        notificationManagerCompat.notify(01, notification.build());

    }

    public void bigPictureStyleNotification(String channelId){

        Bitmap picture = BitmapFactory.decodeResource(getResources(), R.drawable.ic_caballeros);

        //Crear notificacion
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_backup)
                .setContentTitle("Basic Notification")
                .setContentText("Basic Notification Description")
                .setColor(Color.GREEN)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(picture)
                );

        notificationManagerCompat.notify(01, notification.build());
    }

    public void inboxStyleNotification(String channelId){
        //Crear notificacion
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_backup)
                .setContentTitle("Basic Notification")
                .setContentText("Basic Notification Description")
                .setColor(Color.GREEN)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.InboxStyle()
                        .addLine("01")
                        .addLine("02")
                        .addLine("03")
                        .addLine("04")

                );

        notificationManagerCompat.notify(01, notification.build());
    }


}
